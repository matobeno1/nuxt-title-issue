![img](https://media.giphy.com/media/STTBeNlX49QtMzFDDE/source.gif)

Here is the recorded problem, all I do is hard reload `CTRL + SHIFT + R` on Firefox or `CTRL + Click` to open in
the new window. This results in the repetition (2+, even 15 or so times) of the `" - MyApp"` part of the title.

As if there was something storing the whole site title into the `%s`.

```
// nuxt.config.js

head: {
    titleTemplate: '%s - MyApp',
    title: 'TheTitle',
    ...
}
``` 
