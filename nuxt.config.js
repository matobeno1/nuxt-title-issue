import vuetify from './vuetify.config'
import manifest from './manifest.config'
import toast from './toast.config'
import workbox from './workbox.config'

require('dotenv').config() // Adds ability to use .env.[ mode ] files for environment variables

export default {
  debug: process.env.NODE_ENV !== 'production',
  mode: 'spa',
  /*
  ** SSR
   */
  generate: {
    exclude: [
      /^\/app(\/.*)?$/, // exclude the app part - that should be client side only
    ]
  },
  /*
   ** Headers of the page
   */
  router: {
    linkActiveClass: 'active-link',
    linkExactActiveClass: 'exact-active-link',
    middleware: ['auth', 'vue-i18n']
  },

  head: {
    titleTemplate: '%s - MyApp',
    title: 'TheTitle',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: 'icons/favicon.ico'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/icon?family=Material+Icons'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700&display=swap&subset=latin-ext'
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap'
      }
    ],
    script: []
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: ['@/assets/scss/style.scss', 'vuetify/dist/vuetify.min.css'],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    '~plugins/vue-i18n.js',
    '~plugins/axios.js',
    '~plugins/requests.js',
    '~plugins/validation-rules.js',
    '~plugins/global-components.js',
    { src: '~plugins/vuex-persist.js', ssr: false }
  ],
  /*
   ** Nuxt.js build-modules
   */
  buildModules: [
    [
      '@nuxtjs/vuetify',
    ]
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/style-resources',
    '@nuxtjs/toast',
    ['@nuxtjs/moment', { locales: ['sk'], defaultLocale: 'sk' }], // Do not modify - it already is locale free
    ['@nuxtjs/dotenv', { filename: `.env.${process.env.NODE_ENV}` }]
  ],
  styleResources: {
    scss: [
      /* YOU SHOULD PUT HERE ONLY SASS VARIABLES SO THAT YOU DONT NEED TO IMPORT THEM*/
      '~/assets/scss/_variables.scss',
      '~/assets/scss/_colors.scss',
      '~/assets/scss/_mixins.scss',
      '~/assets/scss/breakpoints/_breakpoints.scss'
    ]
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  workbox,
  vuetify,
  manifest,
  toast,

  /*
   ** Build configuration
   */
  build:
    process.env.NODE_ENV === 'production' // Hot reload slows down when using these configs (~450ms = good)
      ? {
          quiet: false, // https://github.com/nuxt/nuxt.js/issues/5623#issuecomment-511733772
          extractCSS: true,
          friendlyErrors: true,
          vendor: [], // https://github.com/nuxt/nuxt.js/issues/439 //['axios', 'vuetify']
          analyze: true,
          splitChunks: {
            layouts: true,
            pages: true,
            commons: true
          },

          extend(config, ctx) {}
        }
      : {
          // filenames: { // DO NOT USE IN DEV MODE
          //   app: '[name].[hash].js',
          //   chunk: '[name].[hash].js'
          // },
          extend(config, ctx) {}
        }
}
