import colors from 'vuetify/es5/util/colors'
const primary_color = '#F15C27'

export default {
  customVariables: ['~/assets/variables.scss'],
  treeShaking: true,
  treeShake: true,
  font: {
    family: 'Montserrat'
  },
  icons: {
    iconfont: 'md'
  },
  theme: {
    dark: false,
    themes: {
      dark: {
        primary: primary_color,
        accent: colors.grey.darken3,
        secondary: colors.amber.darken3,
        info: colors.teal.lighten1,
        warning: colors.amber.base,
        error: colors.deepOrange.accent4,
        success: colors.green.accent3
      },
      light: {
        primary: primary_color,
        accent: colors.grey.darken3,
        secondary: colors.amber.darken3,
        info: colors.teal.lighten1,
        warning: colors.amber.base,
        error: colors.deepOrange.accent4,
        success: colors.green.accent3
      }
    }
  }
}
