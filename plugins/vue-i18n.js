import Vue from 'vue'
import VueI18n from 'vue-i18n'
Vue.use(VueI18n)

import en from "../locales/en"
import sk from "../locales/sk"

export default ({ app, store }) => {


  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch

  // console.log("STORE",store);

  app.i18n = new VueI18n({
    // locale: store.state.i18n.locale,
    locale: store.state.i18n.locale,
    fallbackLocale: store.state.i18n.fallbackLocale,
    messages: {
      // en,
      // sk,
    'en-US': en,
    'sk-SK': sk
    }
  })

  // app.i18n.path = (link) => {
  //   if (app.i18n.locale === app.i18n.fallbackLocale) {
  //     return `/${link}`
  //   }
  //
  //   return `/${app.i18n.locale}/${link}`
  // }
}
