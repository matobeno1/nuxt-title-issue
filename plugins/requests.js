import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'

const API_ROOT = process.env.VUE_APP_API_ROOT // Should end with a slash ('/')

if (!API_ROOT) {
  throw new ReferenceError('API root is not defined - requests.js')
}

if (API_ROOT.slice(-1) !== '/') {
  throw new Error("API_ROOT doesn't end with a slash.")
}

const headers = {
  'Content-Type': 'application/json'
}

class Request {
  static test = () => {
    return axios({
      method: 'GET',
      url: 'https://jsonplaceholder.typicode.com/todos/'
    })
  }

  /**
   Logs the user in, returns JWT logIn in response data.
   https://documenter.getpostman.com/view/8324067/SVYrtysT?version=latest#a3317d8d-43a5-4d76-b555-232f9459856c
   */
  static logIn(data) {
    return axios({
      method: 'POST',
      url: `${API_ROOT}token/`,
      data: { ...data, user_type: 'user' },
      timeout: 10000,
      headers
    })
  }

  /**
   * Logs user out - current device
   */
  static logOut(token) {
    this.assertToken(token)
    return axios({
      method: 'GET',
      url: `${API_ROOT}revokeToken`,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
  }

  /**
   * Logs user out - from all devices
   */
  static logOutGlobal(token) {
    this.assertToken(token)
    return axios({
      method: 'GET',
      url: `${API_ROOT}revokeAllTokens`,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
  }

  static getCities(id, params) {
    return axios({
      method: 'GET',
      url: `${API_ROOT}helpers/countries/${id}/cities`,
      params: params,
      headers: {
        'Content-Type': 'application/json'
      }
    })
  }

  static assertToken(token) {
    if (!token) {
      throw new ReferenceError('No token provided.')
    }
  }

  static getAccountInfo(token) {
    this.assertToken(token)
    return axios({
      method: 'GET',
      url: `${API_ROOT}account`,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    })
  }

  static updateAccountInfo(token, data) {
    this.assertToken(token)
    return axios({
      method: 'PUT',
      url: `${API_ROOT}account`,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      },
      data
    })
  }

  static uploadAvatar(token, data, cancelToken, progressFn) {
    this.assertToken(token)
    return axios({
      method: 'POST',
      url: `${API_ROOT}account/avatar/`,
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      timeout: 10000,
      cancelToken,
      data,
      onUploadProgress: e => {
        progressFn(e.lengthComputable, e.loaded, e.total)
      }
    })
  }

  static forgottenPassword(email) {
    return axios({
      method: 'POST',
      url: `${API_ROOT}forgottenPassword/generate/`,
      headers,
      data: {
        email: email,
        user_type: 'user'
      }
    })
  }

  static redeemPassword(data) {
    return axios({
      method: 'POST',
      url: `${API_ROOT}forgottenPassword/redeem/`,
      headers,
      data: { ...data, user_type: 'user' }
    })
  }
}

/*
  You can use `this.$request` anywhere in the Nuxt app.
 */
Vue.prototype.$request = Request

// Also make it available for the vuex store.
Vuex.Store.prototype.$request = Request
