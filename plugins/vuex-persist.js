import VuexPersistence from 'vuex-persist'
import Cookies from 'js-cookie'
import $moment from 'moment'

const authCookie = new VuexPersistence({
  key: 'my-app',
  restoreState: (key, storage) => {
    let auth = Cookies.getJSON(key)
    return { auth } // Need to return nested object {auth: JSONdata}
  },
  saveState: (key, state, storage) => {
    let expiryDate = null

    try {
      if (state.validTo) {
        expiryDate = $moment(state.validTo).toDate()
      }
    } catch (e) {
      console.error(e)
    }

    Cookies.set(key, state, {
      expires: expiryDate || 14 // or 14 days
    })
  },
  reducer: state => ({
    authToken: state.auth.authToken,
    validTo: state.auth.validTo
  })
})

export default ({ store, $moment }) => {
  authCookie.plugin(store)

  // LOCAL STORAGE
  new VuexPersistence({
    key: 'my-app',
    storage: window.localStorage,
    modules: ['i18n', 'order-form', 'account']
  }).plugin(store)

  // window.onNuxtReady(() => {
  //   authCookie.plugin(store)
  //
  //   // LOCAL STORAGE
  //   new VuexPersistence({
  //     key: 'my-app',
  //     storage: window.localStorage,
  //     modules: ['i18n', 'order-form']
  //   }).plugin(store)
  // })
}

// Code from version 1.

// const vuexCookie = new VuexPersistence({
//   key: "vuex",
//   restoreState: (key, storage) => Cookies.getJSON(key),
//   saveState: (key, state, storage) =>
//     Cookies.set(key, state, {
//       expires: 14
//     }),
//
//   reducer: state => ({
//     auth: state.auth,
//     user: state.user,
//
//   })
// });
//
// const vuexSession = new VuexPersistence({
//   key: 'my-app',
//   storage: window.sessionStorage,
//   reducer: state => ({
//     uuid: state.uuid
//   }),
// })
//
// const vuexLocal = new VuexPersistence({
//   key: 'vuex',
//   storage: window.localStorage,
//   reducer: state => ({
//     orders: state.ordersModule.orders,
//     notifications: state.notifications, // state.notifications.notifications breaks things ...
//     conversations: state.conversations,
//     locale: state.langModule.locale,
//   }),
// });
