import Vue from 'vue'

/*
  You can use `this.$request` anywhere in the Nuxt app.
 */

const unicodeNameRegex = /^[\w'\-,.][^0-9_!¡?÷¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]]+$/
const phoneRegex = /^\+(421|420)[0-9]{9}$/
const uuidRegex = /^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$/

export default ({ app },inject) => {
  const i18n = app.i18n

  // You can use `this.$request` anywhere in the Nuxt app.
  const $rules = {
    name: [
      v => !!v || 'Name is required',
      v => v.length >= 2 || 'Name is too short',
      v => unicodeNameRegex.test(v) || 'Name not valid'
    ],
    surname: [
      v => !!v || 'Surname is required',
      v => v.length >= 2 || 'Surname is too short',
      v => unicodeNameRegex.test(v) || 'Surname not valid'
    ],
    email: [
      v => !!v || 'E-mail is required',
      v => /.+@.+/.test(v) || 'E-mail must be valid'
    ],
    password: [
      v => !!v || 'Password is required',
      v => v.length >= 8 || 'Password must be at least 8 characters long.'
    ],
    phoneMask: [
      // This validates phone number with dashes (918-281-525), true for null number
      v =>
        /^([1-9][0-9]{2}-[0-9]{3}-[0-9]{3})?$/.test(v) ||
        'Phone number is not valid'
    ],
    phone: [
      v => phoneRegex.test(v) || 'Phone number not valid'
    ],
    required: [v => !!v || i18n.t('required')],
    uuidRegex,
  }

  Vue.prototype.$rules = $rules
  inject('rules', $rules)
}
