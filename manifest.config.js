// /*
// PWA manifest
//  */

export default {
  name: 'MyApp',
  short_name: 'MyApp',
  start_url: '/login', // Make sure this is not null or the browser web will open
  display: 'standalone', // I want the app to look like native app (other options: fullscreen, browser)
  background_color: '#F15C27',
  theme_color: '#F15C27',
  orientation: 'portrait-primary',
  lang: 'en',
  description: 'The description.',
}
