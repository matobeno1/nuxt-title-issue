import { authRequired, requiredPatterns } from '../middleware/auth'

describe('Auth pattern matching', () => {
  test('is true for `app` (base route)', () => {
    expect(authRequired(requiredPatterns, 'app')).toBeTruthy()
    expect(authRequired(requiredPatterns, 'App')).toBeTruthy()
    expect(authRequired(requiredPatterns, 'aPP')).toBeTruthy()
  })
  test('is true for `app/create-order``', () => {
    const routeName = 'app-create_order'
    const result = authRequired(requiredPatterns, routeName)
    expect(result).toBeTruthy()
  })

  test('is true for `app/user``', () => {
    const routeName = 'app-user'
    const result = authRequired(requiredPatterns, routeName)
    expect(result).toBeTruthy()
  })

  test('is false for `index` page`', () => {
    const routeName = 'index'
    const result = authRequired(requiredPatterns, routeName)
    expect(result).toBeFalsy()
  })

  test('is false for `faq` page`', () => {
    const routeName = 'faq'
    const result = authRequired(requiredPatterns, routeName)
    expect(result).toBeFalsy()
  })
})
