/*
  @nuxtjs/toasted
   */
export default {
  position: 'bottom-right',
  duration: 3000,
  keepOnHover: true,
  iconPack: 'mdi',
  className: 'toasted-notification',
  register: [
    // Register custom toasts
    // {
    //   name: 'my-error',
    //   message: 'Oops...Something went wrong',
    //   options: {
    //     type: 'error'
    //   }
    // }
  ]
}
