export default function({ isHMR, app, store, route, params, error, redirect }) {
  // If middleware is called from hot module replacement, ignore it
  if (isHMR) {
    console.log('vue-i18n isHMR')
    return
  }

  app.i18n.locale = store.state.i18n.locale
}
