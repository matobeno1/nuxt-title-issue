import Cookies from 'js-cookie'

export default function({ store, route, redirect, $moment, app }) {
  let $toast = app.$toast
  const cookies = Cookies.getJSON('auth')

  if (route.name === 'index') {
    return
  }

  // Already authenticated = can continue
  // Note: the Vuex store getter for isAuthenticated method is undefined - this is a workaround
  if (cookies && cookies.authToken) return


  if (!authRequired(requiredPatterns, route.name)) {
    return
  }

  showLoginRequiredMessage($toast)
  console.warn('Unauth, redirecting to route named `index`')
  redirect({ name: 'login' })
}

export const requiredPatterns = [
  '^app' // Any url beginning in /app - aka any page in the app folder.
]

export const authRequired = (requiredPatterns, routeName) => {
  let re = new RegExp(requiredPatterns.join('|'), 'i') // 'i' for case insensitive
  return re.test(routeName)
}

const showLoginRequiredMessage = $toast => {
  $toast.show('Login needed', {
    duration: 1500,
    singleton: true
  })
}
