export const state = () => ({
  opened: false
})
export const getters = {}
export const actions = {}
export const mutations = {
  setOpen(state, boolean) {
    state.opened = boolean
  }
}
