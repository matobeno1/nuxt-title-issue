export const state = () => ({
  locales: ['en-US', 'sk-SK'],
  locale: 'sk-SK',
  fallbackLocale: 'sk-SK'
})
export const getters = {
  getLocale(state) {
    return state.locale
  },

  getAvailableLocales: state =>
    state.locales.filter(lang => {
      return lang !== state.locale
    })
}
export const mutations = {
  /**
   * Sets variable in the store
   * and the nuxt.app.i18n object because for some reason it doesn't work
   * within the plugin when getting value via `store.state.i18n.locale`.
   * See the plugins/vue-i18n.
   */
  switchLocale(state, locale) {
    if (state.locales.includes(locale)) {
      state.locale = this.app.i18n.locale = locale
      this.$moment.locale(locale)
      return
    }

    console.warn(`switching locale failed, fallback to ${state.fallbackLocale}`)
    state.locale = this.app.i18n.locale = state.fallbackLocale
  }
}

export const actions = {}
