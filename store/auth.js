export const state = () => ({
  authToken: null,
  validTo: null
})
export const getters = {
  isAuthenticated(state) {
    return !!state.authToken
  },

  getAuthToken(state) {
    return state.authToken || null
  }
}
export const mutations = {
  setToken(state, payload) {
    state.authToken = payload.access_token
    state.validTo = payload.valid_to
  },
  wipeToken(state) {
    state.authToken = null
    state.validTo = null
  }
}
export const actions = {
  /**
   Sends request with credentials, receives object with JWT
   and saves it to store. It also starts fetching of all data about user.
   */
  async logIn(context, payload) {
    await this.$request
      .logIn(payload)
      .then(res => {
        context.commit('setToken', res.data)
        this.dispatch('account/fetchData')
      })
      .catch(err => {
        // If err response has error text in nice form from the server:
        if (err.response) {
          throw err.response.data.msg || 'Unable to log in.'
        }
        // Otherwise unknown err
        console.error(err)
        throw err
      })
  },

  /**
   * Log user out of this device, before redirecting to main page,
   * wipes out all user account data and token.
   */
  async logOut(context) {
    const token = this.state.auth.authToken || null
    await this.$request
      .logOut(token)
      .then(res => {
        context.commit('wipeToken')
        this.$toast.success(res.data.msg)
        this.$router.push({ name: 'index' }, () =>
          this.commit('account/wipeUserData')
        )
      })
      .catch(err => {
        let errorMessage = 'Unable to log out.'
        if (err.response && err.response.data.msg) {
          errorMessage = err.response.data.msg
        }
        console.error(`Unable to log out: ${err}`)
        this.$toast.error(errorMessage)
      })
  },

  /**
   * Almost identical to `logOut`. (Sorry, not DRY, not enought time)
   * // TODO optimize based on DRY
   */
  async logOutAll(context) {
    const token = this.state.auth.authToken || null
    await this.$request
      .logOut(token)
      .then(res => {
        context.commit('wipeToken')
        this.$toast.success(res.data.msg)
        this.$router.push({ name: 'index' }, () =>
          this.commit('account/wipeUserData')
        )
      })
      .catch(err => {
        let errorMessage = 'Unable to log out from all devices.'
        if (err.response && err.response.data.msg) {
          errorMessage = err.response.data.msg
        }
        console.error(`Unable to log out: ${err}`)
        this.$toast.error(errorMessage)
      })
  }
}
