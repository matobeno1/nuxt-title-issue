export const state = () => ({
  orderType: null, // 'transport'/'moving'
  addressOrigin: {
    street: null,
    number: null,
    city: null,
    country: null
  },
  addressDestination: {}
})
export const getters = {
  getAddressOrigin(state) {
    return state.addressOrigin
  },
  getAddressDestination(state) {
    return state.addressDestination
  }
}
export const mutations = {
  setOrderType(state, payload) {
    state.orderType = payload
  },
  setAddressOrigin(state, payload) {
    state.addressOrigin = { ...payload }
  },

  setAddressDestination(state, payload) {
    state.addressDestination = { ...payload }
  }
}
export const actions = {}
